package com.vangiex.facebook;

import android.content.Context;
import android.util.Log;

import com.facebook.ads.Ad;
import com.facebook.ads.InterstitialAd;
import com.vangiex.facebook.adlistener.InterstitialListener;

public class Interstitial {

    private final InterstitialAd interstitialAd;
    private String UnitId;

    public Interstitial(Context ctx,String UnitId) {
        this.UnitId = UnitId;
        interstitialAd = new InterstitialAd(ctx,UnitId);
        interstitialAd.setAdListener(new Listener("interstitial".toUpperCase()));
        interstitialAd.loadAd();
    }

    public Interstitial(Context ctx,String UnitId,Boolean FLAG_AUTOSHOW) {
        this.UnitId = UnitId;
        interstitialAd = new InterstitialAd(ctx,UnitId);
        if(FLAG_AUTOSHOW)
        {
            interstitialAd.setAdListener(new Listener("interstitial".toUpperCase()));
        }
        interstitialAd.loadAd();
    }

    public void show()
    {
        interstitialAd.show();
    }

    public String getUnitId() {
        return UnitId;
    }


    class Listener extends InterstitialListener {

        public Listener(String TAG) {
            super(TAG);
        }

        @Override
        public void onAdLoaded(Ad ad) {
            // Interstitial ad is loaded and ready to be displayed
            Log.d(TAG, "ad is loaded and ready to be displayed!");
            // Show the ad
            interstitialAd.show();
        }

       }
}
