package com.vangiex.facebook.nativead;

import android.content.Context;
import android.widget.LinearLayout;

import com.facebook.ads.NativeBannerAd;
import com.facebook.ads.NativeBannerAdView;

public class NativeBannerTemplate {

    private final NativeBannerAdView.Type SIZE;
    private final Context ctx;
    private final String ID;
    private final LinearLayout view;

    private NativeBannerAd mNativeBannerAd;
    public static NativeBannerAdView.Type SIZE_100 = NativeBannerAdView.Type.HEIGHT_100;
    public static NativeBannerAdView.Type SIZE_120 = NativeBannerAdView.Type.HEIGHT_120;


    public NativeBannerTemplate(Context ctx, String ID, LinearLayout view, NativeBannerAdView.Type SIZE) {
        this.ctx = ctx;
        this.ID = ID;
        this.SIZE = SIZE;
        this.view = view;

        Init();
        AddListener();
        LoadAd();

    }

    private void Init()
    {
        mNativeBannerAd = new NativeBannerAd(ctx, ID);
    }

    private void AddListener()
    {
        mNativeBannerAd.setAdListener(new NBListener(ctx,mNativeBannerAd,view,SIZE));
    }

    private void LoadAd()
    {
        mNativeBannerAd.loadAd();
    }



}
