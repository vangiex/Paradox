package com.vangiex.facebook.nativead;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeBannerAd;
import com.facebook.ads.NativeBannerAdView;

class NBListener implements NativeAdListener {


    private Context ctx;
    private NativeBannerAd mNativeBannerAd;
    private LinearLayout nativeBannerAdContainer;
    private NativeBannerAdView.Type SIZE;

    public NBListener(Context ctx, NativeBannerAd mNativeBannerAd, LinearLayout nativeBannerAdContainer, NativeBannerAdView.Type SIZE) {
        this.ctx = ctx;
        this.mNativeBannerAd = mNativeBannerAd;
        this.nativeBannerAdContainer = nativeBannerAdContainer;
        this.SIZE = SIZE;
    }

    @Override
    public void onError(Ad ad, AdError adError) {

    }

    @Override
    public void onAdLoaded(Ad ad) {

        View adView = NativeBannerAdView.render(ctx,mNativeBannerAd , SIZE);
        // Add the Native Banner Ad View to your ad container
        nativeBannerAdContainer.addView(adView);

    }

    @Override
    public void onAdClicked(Ad ad) {

    }

    @Override
    public void onLoggingImpression(Ad ad) {

    }

    @Override
    public void onMediaDownloaded(Ad ad) {

    }
}
