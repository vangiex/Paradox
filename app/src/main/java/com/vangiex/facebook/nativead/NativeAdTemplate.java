package com.vangiex.facebook.nativead;

import android.content.Context;
import android.widget.RelativeLayout;

import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdView;

public class NativeAdTemplate {

    private final NativeAdView.Type SIZE;
    private final Context ctx;
    private final String ID;
    private final RelativeLayout view;

    private NativeAd nativeAd;

    public static NativeAdView.Type SIZE_400 = NativeAdView.Type.HEIGHT_400;
    public static NativeAdView.Type SIZE_300 = NativeAdView.Type.HEIGHT_300;

    public NativeAdTemplate(Context ctx, String ID, RelativeLayout view, NativeAdView.Type SIZE) {
        this.ctx = ctx;
        this.ID = ID;
        this.SIZE = SIZE;
        this.view = view;

        Init();
        AddListener();
        LoadAd();

    }

    private void Init()
    {
        nativeAd = new NativeAd(ctx, ID);
    }

    private void AddListener()
    {
        nativeAd.setAdListener(new NListener(ctx, nativeAd, view , SIZE));
    }

    private void LoadAd()
    {
        nativeAd.loadAd();
    }




}