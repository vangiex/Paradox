package com.vangiex.facebook.nativead;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeAdView;


class NListener implements NativeAdListener {
    private Context ctx;
    private NativeAdView.Type SIZE;
    private RelativeLayout nativeAdContainer;
    private NativeAd nativeAd;

    public NListener(Context ctx, NativeAd nativeAd, RelativeLayout nativeAdContainer, NativeAdView.Type SIZE) {
        this.ctx = ctx;
        this.SIZE = SIZE;
        this.nativeAd = nativeAd;
        this.nativeAdContainer = nativeAdContainer;
    }

    @Override
    public void onMediaDownloaded(Ad ad) {

    }

    @Override
    public void onError(Ad ad, AdError adError) {

    }



    @Override
    public void onAdLoaded(Ad ad) {
        // Render the Native Ad Template
        View adView = NativeAdView.render(ctx, nativeAd, SIZE);
        // Add the Native Ad View to your ad container
        nativeAdContainer.addView(adView);
    }

    @Override
    public void onAdClicked(Ad ad) {

    }

    @Override
    public void onLoggingImpression(Ad ad) {

    }
}

