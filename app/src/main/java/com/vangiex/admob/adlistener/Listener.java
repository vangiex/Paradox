package com.vangiex.admob.adlistener;

import android.util.Log;
import com.google.android.gms.ads.AdListener;

public class Listener extends AdListener {

    private String TAG = "ADMOB";
    private CustomAction listener = null;

    public Listener(String TAG) {
                this.TAG = this.TAG + TAG;
        }

    public Listener(String TAG, CustomAction listener) {
        this.listener = listener;
        this.TAG = this.TAG + TAG;
    }

    public String getTAG() { return TAG; }

    public void setTAG(String TAG) { this.TAG = TAG;}

    @Override
    public void onAdLoaded() {
        // Code to be executed when an ad finishes loading.
        Log.d(getTAG(), "ad is loaded and ready to be displayed!");
        if(listener != null) {
            Log.d("CustomAction","CustomAction.onAdLoaded();");
            listener.onAdLoaded();
        }
    }

    @Override
    public void onAdFailedToLoad(int errorCode) {
        // Code to be executed when an ad request fails.
        Log.e(TAG, "ad failed to load ErrorCode :  " + String.valueOf(errorCode));
        if(listener != null) {
            Log.d("CustomAction","CustomAction.onAdLoaded();");
            listener.onAdLoaded();
        }
        }

        @Override
        public void onAdOpened() {
            // Code to be executed when an ad opens an overlay that
            // covers the screen.
                Log.d(getTAG(), "ad opened!!");
            if(listener != null) {
                Log.d("CustomAction","CustomAction.onAdOpened();");
                listener.onAdLoaded();
            }
        }

        @Override
        public void onAdLeftApplication() {
            // Code to be executed when the user has left the app.
                Log.d(getTAG(), "ad leftappication");
            if(listener != null) {
                Log.d("CustomAction","CustomAction.onAdLeftApplication();");
                listener.onAdLoaded();
            }
        }

        @Override
        public void onAdClosed() {
            // Code to be executed when when the user is about to return
            // to the app after tapping on an ad.
                Log.d(getTAG(), "ad closed!!");
            if(listener != null) {
                Log.d("CustomAction","CustomAction.onAdClosed();");
                listener.onAdLoaded();
            }
        }
}
