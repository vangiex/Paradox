package com.vangiex.admob.adlistener;

public interface CustomAction {

    public void onAdLoaded();

    public void onAdFailedToLoad(int errorCode);

    public void onAdOpened();

    public void onAdLeftApplication();

    public void onAdClosed();
}
