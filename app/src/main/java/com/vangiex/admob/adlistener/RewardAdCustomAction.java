package com.vangiex.admob.adlistener;

import com.google.android.gms.ads.reward.RewardItem;

public interface RewardAdCustomAction {

    public void onRewardedVideoAdLoaded();

    public void onRewardedVideoAdOpened();

    public void onRewardedVideoStarted();

    public void onRewardedVideoAdClosed();

    public void onRewarded(RewardItem rewardItem);

    public void onRewardedVideoAdLeftApplication();

    public void onRewardedVideoAdFailedToLoad(int i);
}
