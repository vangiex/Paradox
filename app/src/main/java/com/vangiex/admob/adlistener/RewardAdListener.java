package com.vangiex.admob.adlistener;

import android.util.Log;

import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class RewardAdListener implements RewardedVideoAdListener {

    String TAG = "ADMOB";

    public RewardAdListener(String TAG) {
        this.TAG = this.TAG + TAG;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.d(getTAG(), "ad loaded!");
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.d(getTAG(), "ad opened!");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.d(getTAG(), "ad started!");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.d(getTAG(), "ad closed!");
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.d(getTAG(), "ad onreward!");
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.d(getTAG(), "ad left application!");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.d(getTAG(), "ad failed! code : " + String.valueOf(i));
    }
}

