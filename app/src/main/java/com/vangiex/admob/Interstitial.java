package com.vangiex.admob;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.ads.InterstitialAd;
import com.vangiex.admob.adlistener.CustomAction;
import com.vangiex.admob.adlistener.Listener;
import com.vangiex.admob.adrequest.Request;

import java.util.concurrent.ExecutionException;


public class Interstitial {

    private InterstitialAd mInterstitialAd;
    private String unitId;
    private Context ctx;
    private String TAG = "INTERSTITIAL";

    // Mode AutoShow
    public Interstitial(Context ctx, String unitId) throws ExecutionException, InterruptedException {
        Init(ctx,unitId);
        RequestAd();
        Addlistener();
    }

    // Mode ManullyShow
    public Interstitial(Context ctx , String unitId , Boolean FLAG_AUTOSHOW) throws ExecutionException, InterruptedException {
        Init(ctx,unitId);
        RequestAd();
        if(FLAG_AUTOSHOW){ Addlistener(); }
    }

    // Mode Manull + Fab
    public Interstitial(Context ctx , String unitId , CustomAction lstn , Boolean FLAG_AUTOSHOW) throws ExecutionException, InterruptedException {
        Init(ctx,unitId);
        RequestAd();
        Addlistener(lstn);
        if(FLAG_AUTOSHOW){ show(); }
    }



    // INIT
    public void Init(Context ctx , String unitId) {
        this.unitId = unitId;
        this.ctx = ctx;
        mInterstitialAd = new InterstitialAd(ctx);
        mInterstitialAd.setAdUnitId(unitId);
    }

    // ADMOB REQUESt
    public void RequestAd() throws ExecutionException, InterruptedException {
        mInterstitialAd.loadAd(new Request().execute().get());
    }

    // Mannully Show Ad
    public void show() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            Log.d("ADMOBInterstitial", "Manully Show Called");
        } else {
            Log.e("ADMOBInterstitial", "The interstitial wasn't loaded yet.");
        }
    }

    // Regular Listener
    public void Addlistener() {
        mInterstitialAd.setAdListener(new IListener(TAG));
    }

    // CustomListener
    public void Addlistener(CustomAction Listener) {
        mInterstitialAd.setAdListener(new Listener(TAG , Listener));
    }

    // Custom Listener For autoshow
    class IListener extends Listener {

        public IListener(String TAG) {
            super(TAG);
        }

        @Override
        public void onAdLoaded() {
            // Code to be executed when an ad finishes loading.
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                super.onAdLoaded();
            } else {
                Log.d(getTAG(), "The interstitial wasn't loaded yet.");
            }
        }
    }
}

