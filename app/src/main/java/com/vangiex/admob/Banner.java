package com.vangiex.admob;


import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.vangiex.admob.adlistener.CustomAction;
import com.vangiex.admob.adlistener.Listener;
import com.vangiex.admob.adrequest.Request;

import java.util.concurrent.ExecutionException;

public class Banner {
    private AdRequest Request;
    private AdView view;
    private Context ctx;
    private String TAG = "BANNER";

    // Mode : auto show
    public Banner(Context ctx,View viewById) throws ExecutionException, InterruptedException {
        Init(ctx,viewById);
        Addlistener();
        show();
    }

    // Mode : manully show
    public Banner(Context ctx,View viewById , Boolean FLAG_AUTOSHOW){
        Init(ctx,viewById);
        if(FLAG_AUTOSHOW) { Addlistener(); }
    }

    public Banner(Context ctx,View viewById,CustomAction lstn , Boolean FLAG_AUTOSHOW) throws ExecutionException, InterruptedException {

        Init(ctx,viewById);
        Addlistener(lstn);
        if(FLAG_AUTOSHOW) { show();}

    }

    // Init View
    private void Init(Context ctx,View viewById) {
        this.ctx = ctx;
        this.view = (AdView) viewById;
    }

    // Request Admob New Ad
    private AdRequest getRequest() throws ExecutionException, InterruptedException {
        return new Request().execute().get();
    }

    // Mannully Show Ad
    public void show() throws ExecutionException, InterruptedException {
        view.loadAd(getRequest());
    }

    // PreBuilt Listener Regular
    private void Addlistener() {
        view.setAdListener(new Listener(TAG));
    }


    // Custom Listener
    public void Addlistener(CustomAction Listener) {
        view.setAdListener(new Listener(TAG , Listener));
    }

}
